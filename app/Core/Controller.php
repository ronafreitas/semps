<?php

namespace App\Core;

class Controller{
	
	public static $url_redirect = 'http://localhost/semps/suas/';
	
	public function __construct(){
		/*try{
			return $_SESSION;
		}catch(Throwable $t){
			return $t;
		}*/

		/*if(!isset($_SESSION)){
			return false;
		}else{
			return $_SESSION;
		}*/
	}

	public static function urlRedirect(string $url=''){
		header('Location: '.self::$url_redirect.$url);
		die();
	}
}