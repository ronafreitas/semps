<?php

namespace App\Core;

class Db{

	private static $host = '127.0.0.1';
	private static $dbname = 'semps';
	private static $user = 'root';
	private static $password = '123';

    public static function getDb(){
		try{
		    $dbh = new \PDO("mysql:host=".self::$host.";dbname=".self::$dbname,self::$user,self::$password, array(
			    \PDO::ATTR_PERSISTENT => true
			));
			//$dbh->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		    return $dbh;
		}catch (\PDOException $e) {
		    return $e->getMessage();
		    die();
		}
    }
}
