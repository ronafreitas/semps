<?php

namespace App\Core;

class Rotas{

	private $rotas;
	public static $rootUrl='http://localhost/semps/suas';

	public function __construct(){
		$array_rotas = [];

		// login
		$array_rotas['login']	= [
			['action'=>'indexLogin','controller'=>'LoginController'],
			['action'=>'sairLogin','controller'=>'LoginController'],
			['action'=>'erroLogin','controller'=>'LoginController'],
			['action'=>'acessarLogin','controller'=>'LoginController'],
		];

		// home
		$array_rotas['home']	= [
			['action'=>'indexHome','controller'=>'IndexController'],
			['action'=>'listarUm','controller'=>'IndexController'],
			['action'=>'listarTodos','controller'=>'IndexController'],
			['action'=>'insereUm','controller'=>'IndexController'],
		];

		// usuarios
		$array_rotas['usuario']	= [
			['action'=>'indexUsuario','controller'=>'UsuarioController'],
			['action'=>'novoUsuario','controller'=>'UsuarioController'],
			['action'=>'listarUm','controller'=>'UsuarioController'],
			['action'=>'cadastrarUsuario','controller'=>'UsuarioController'],
			['action'=>'umUsuario','controller'=>'UsuarioController'],
			['action'=>'atualizaUsuario','controller'=>'UsuarioController'],
			['action'=>'deletarUsuario','controller'=>'UsuarioController'],
			//['action'=>'umUsuario','metodoUUUU'=>'UsuarioController::metodo'],
		];

		$this->rotas = $array_rotas;
	}

	public function getUrl(string $url, string $action, array $data){
		$retn=false;
		if(isset($this->rotas[$url])){
			foreach($this->rotas[$url] as $rota){
				if($rota['action'] == $action){
					$retn = $rota;
				}
			}
		}
		return $retn;
	}

	public function redirecionarRoot(){
		header('Location: '.self::$rootUrl);
		exit();
	}

	public function redirecionarHome(){
		header('Location: '.self::$rootUrl.'/home/indexHome');
		exit();
	}

	public function redirecionarErroLogin(){
		header('Location: '.self::$rootUrl.'/login/erroLogin');
		exit();
	}

	public function validaSessaoToken(){
		if(isset($_POST['input']['token'])){
			if($_POST['input']['token'] !== $_SESSION['token']){
				$_SESSION = null;
				session_unset();
				session_destroy();
				self::redirecionarRoot();
			}
		}

		if(isset($_SESSION['expira_sessao'])){
			if($_SESSION['expira_sessao'] < (time() - 1800) ){
				$_SESSION = null;
				session_unset();
				session_destroy();
				self::redirecionarRoot();
			}
		}
	}

}