<?php

namespace App\Core;

class Utils{
	
	public static function validaEmail(string $email):bool{
		return filter_var($email, FILTER_VALIDATE_EMAIL);
	}

	public static function validaInteiro(int $int):bool{
		return filter_var($int, FILTER_VALIDATE_INT);
	}

}