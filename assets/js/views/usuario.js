"use strict";

class Usuario{

    constructor(rootUrl){
    	this.rootUrl = rootUrl;
    }

    deletarUsuario(idUsuario){
        try{
            var ts = this;
            document.getElementById("apagarUsuario").onclick = function(){
                console.log( document.getElementById(idUsuario).value );
                var id = document.getElementById(idUsuario).value;
                if(!document.getElementById(idUsuario).value){
                    return false;
                }
                id = parseInt(id);
                location.href = ts.rootUrl+'/usuario/deletarUsuario?id='+id;
            };
        }
        catch(err){
            //alert('Não será possível realizar essa ação');
            console.error(err);
            return null;
        }
    }

}
