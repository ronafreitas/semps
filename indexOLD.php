<?php
/*
	Se você está olhando esse arquivo é porque algo muito errado está acontecendo
*/
require_once "vendor/autoload.php";

$loadTemeplate = new \Twig_Loader_Filesystem(__DIR__.'/app/Views/');
$twig = new \Twig_Environment($loadTemeplate);
//$twig = new \Twig_Environment($loadTemeplate, ['cache' => __DIR__.'/app/Views/_cache/']);
session_start();

/*

CSRF 
TOKEN

*/

try{

	$r = new \App\Routers\Router;

	(string) $url 	 = $_GET['url'] ?? 'login';
	(string) $method = $_SERVER['REQUEST_METHOD'] ?? null;
	(string) $action = $_GET['action'] ?? 'indexLogin';
	(array)  $data 	 = $_POST['data'] ?? [];

	$rota		= $r->getUrl($url,$method,$action,$data);
	$classname  = '\App\Controllers\\'.$rota['controller'];
	$instancia  = new $classname;

	if(!isset($_SESSION) OR count($_SESSION) == 0){
		if($url == 'login' AND $action == 'indexLogin'){
			echo $twig->render('login/indexLogin.html');
		}elseif($url == 'login' AND $action == 'acessarLogin'){
			$retorno = $instancia->$action($data);
			if($retorno){
				header('Location: http://localhost/semps/suas/home?action=indexHome');
			}else{
				header('Location: http://localhost/semps/suas/login?action=erroLogin');
			}
		}elseif($url == 'login' AND $action == 'erroLogin'){
			echo $twig->render('login/erroLogin.html', ['mensagem_login' => 'erro ao tentar conectar']);
		}else{
			$instancia->$action();
			header('Location: http://localhost/semps/suas/');
		}
	}else{

		if($url != 'login'){

			/*echo 'url:',$url,'<br/>';
			echo 'action:',$action,'<br/>';
			echo "<pre>"; print_r($_SESSION); echo "</pre>";
			echo "<pre>"; print_r($rota); echo "</pre>";
			echo "<hr/>";*/

			$vld_arg	= new ReflectionMethod($classname, $action);
			$vld_params = $vld_arg->getParameters();

			// verifica se o controller possui parâmetros obrigatórios
			if(count($vld_params) == 0){
				//echo " aquiii <br/>";
				$retorno = $instancia->$action();
				echo $twig->render($url.'/'.$rota['action'].'.html', ['dados' => $retorno]);
			}else{

				if($rota['method'] == 'post'){
					$retorno = $instancia->$action($data);
					if($retorno){

						//$retorno = $instancia->$action();
						//echo $twig->render($url.'/'.$rota['action'].'.html', ['dados' => $retorno]);
						echo "aaaa <br/>";
						echo "<pre>"; print_r($rota); echo "</pre>";
					}else{
						echo "bbb <br/>";
						echo "<pre>"; print_r($rota); echo "</pre>";

					}


				}else{

					echo "<pre>";
					echo "ccc <hr/>";
					$retorno = $instancia->$action($data);
					print_r($retorno);
					print_r($rota);

				}

				/*
					foreach($vld_params as $param){
					    echo 'parametro: ',$param->getName();
					    echo "<br/>";
					    echo 'opcional ',$param->isOptional();
					}
					$saida = $instancia->$action();
					echo $saida;
				*/
			}
		}else{
			$_SESSION = array();
			session_destroy();
			header('Location: http://localhost/semps/suas/home?action=indexHome');
		}
	}


	/*if($url == 'login' AND $action == 'indexLogin'){
		echo $twig->render('login/indexLogin.html', ['dados' => []]);
	}elseif($url == 'login' AND $action == 'acessarLogin'){
		$retorno = $instancia->$action($data);
		if($retorno){
			header('Location: http://localhost/semps/suas/home?action=indexHome');
		}else{

			header('Location: http://localhost/semps/suas/login?action=erroLogin');

			// usario ou senha não conferem
			//echo $twig->render('login/indexLogin.html', ['mensagem_login' => 'bla vla']);
			//exit('usario ou senha não conferem');
		}
	}elseif($url == 'login' AND $action == 'erroLogin'){
		//$instancia->$action();
		echo $twig->render('login/erroLogin.html', ['mensagem_login' => 'erro ao tentar conectar']);
	}elseif($url == 'login' AND $action == 'sairLogin'){
		$instancia->$action();
		header('Location: http://localhost/semps/suas/');
		//echo "sair: <pre>"; print_r($_SESSION); echo "</pre>";



	}else{

	}*/

}catch(Throwable $t){
	$prod_mod=true;
	if($prod_mod){
		$_SESSION = array();
		session_destroy();
		header('Location: http://localhost/semps/suas/');
	}else{
		//echo $twig->render('_erro/404.html', ['erro_mensagem' => 'Erro ao acessar essa página'] );
		//echo $twig->render('_erro/404.html', ['erro_mensagem' => $t->getMessage()] );
		echo "<pre>"; print_r($t);
	}

}
